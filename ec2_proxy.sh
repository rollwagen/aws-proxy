#!/bin/bash

start=$(ec2-run-instances --key rollkey ami-a9184ac0 -t t1.micro --user-data-file ec2_proxy_ubuntu_squid.sh -g ssh_proxy_from_home)

INSTANCE=$(echo "$start" | awk '/^INSTANCE/ {print $2}')


echo $INSTANCE
sleep 20

description=$(ec2-describe-instances $INSTANCE)

dnsname=$(echo $description | awk '/.*/ {print $8}')


dig $dnsname | awk '/^ec2/ {print $5}'

