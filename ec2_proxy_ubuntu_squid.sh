#!/bin/bash

sudo apt-get install -y squid3

sudo sed -i.bak 's/^http_access\ deny\ all/http_access\ allow\ all/' /etc/squid3/squid.conf

sudo service squid3 restart
