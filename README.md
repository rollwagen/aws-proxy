Squid Proxy on AWS EC2 Script
-------------------------

1. `ec2_proxy.sh` - spin up an Ubuntu EC2 image (with pre-set security group)
2. `ec2_proxy_ubuntu_squid.sh` - using cloud-init (i.e. `user-date`)
	* Install `squid`
	* Configure `squid`
	* Restart `squid` 

